# Pawnshop contract

## Status: Feasibility study

An oracle based contract that lets you borrow tokens representing oracle
asset against BCH collateral.

The borrowed tokens would have a "incentivized floating peg" -- there are
built-in market incentives for the borrowed token to be close to the asset
borrowed against.

The contract requires over-collateralization for borrowed asset. When a loan
does not fulfill the collateral requirements (due to BCH price going down),
then the loan becomes "anyone-can-repay" -- anyone willing to repay the tokens
can claim the collateral.

## Incentives

If the price of the borrowed token is too low, existing borrowers are incentivized
to purchase the token from the market to repay their loan for below-marked-value.

If the price of the borrowed token is too high, new borrowers are incentivized
to take loan to profit from overvalued token.

Note that these incentives may not be sufficient. It is likely that in a
bull market that speculators would be willing to borrow "below market value"
to gear their investments. In a bear market, borrowers may want to
unlock their BCH, competing with others seeking "refuge" in the coin.

## Contract

The main contract stores an oracle sequence number in a NFT commitment.
The main contract (pawnshop.cash) has two functions:

* *updateSequence* -- this updates the sequence of the oracle message.
   Someone(TM) has to call this regulary in order for stop malicious borrowers from
   minting tokens based on old price.

* *borrow* -- This lets borrowers lock BCH into a loan contract (loan.cash) and in
             return mint asset token.

The loan contract stores the borrowed amount of asset token in a NFT commitment.
The loan contract has two functions:

* *addCollateral* -- This lets the borrower add more BCH collateral.

* *repayLoan* -- Repay the borrowed tokens and unlock the BCH.
                 If there is enough collateral, only the loan holder can repay.
                 If there is not enough collateral, anyone can repay.
